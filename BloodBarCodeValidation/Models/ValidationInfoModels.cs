﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BloodBarCodeValidation.Models
{
    public class BloodManualInfoModels
    {

        public string FormCode { get; set; }
        public string BagCode { get; set; }
        public string SampleCode { get; set; }
        public string BloodbraidCode { get; set; }
        public string UserId { get; set; }

    }
}