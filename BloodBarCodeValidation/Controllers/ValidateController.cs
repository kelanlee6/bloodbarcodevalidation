﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BloodBarCodeValidation.Models;
using Microsoft.Ajax.Utilities;
using NLog;

namespace BloodBarCodeValidation.Controllers
{
    public class ValidateController : ApiController
    {
        private Logger logger;

        public ValidateController()
        {
            logger = LogManager.GetLogger(this.GetType().ToString());
        }
        // GET: Validate
        [System.Web.Http.HttpPost]
        public HttpResponseMessage Index(BloodManualInfoModels info)
        {
            logger.Log(LogLevel.Info,$"\n登记表：{info.FormCode}\n血袋：{info.BagCode}\n标本：{info.SampleCode}\n辫子：{info.BloodbraidCode}\n操作员：{info.UserId}");
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        
    }

    
}